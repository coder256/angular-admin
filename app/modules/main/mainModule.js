/**
 * Created by Andrew on 9/5/16.
 */

var myApp = angular.module('masikini_admin.main', ['masikini_admin.main.controllers','masikini_admin.main.services']);

myApp.config(['$stateProvider', function ($stateProvider) {
    $stateProvider.state('home',{
        url: '/home',
        abstract:true,
        controller:'HomeController',
        //resolve: {
        //    user: ['userService', '$q', function (userService, $q) {
        //        return userService.user || $q.reject({unAuthorized: true});
        //    }]
        //},
        templateUrl:'modules/main/home.html'
    }).state('home.dashboard',{
        url:'',
        controller:'DashboardController',
        templateUrl: 'modules/main/views/dashboard.html'
    }).state('home.upload',{
        url:'/uploadImages',
        controller:'UploadController',
        templateUrl:'modules/main/views/picUpload.html'
    }).state('home.images',{
        url:'/viewImages',
        controller:'ImageController',
        templateUrl:'modules/main/views/images.html'
    }).state('home.users',{
        url: '/users',
        controller: 'UsersController',
        templateUrl : 'modules/main/views/users.html'
    }).state('home.taobao',{
        url: '/taobao_orders',
        controller: 'TaobaoController',
        templateUrl : 'modules/main/views/taobao.html'
    }).state('home.transactions',{
        url: '/transactions',
        controller: 'TransactionsController',
        templateUrl : 'modules/main/views/transactions.html'
    }).state('home.warehouseUs',{
        url: '/expectedUs',
        controller: 'ExpectedUsController',
        templateUrl : 'modules/main/views/warehouseUs.html'
    }).state('home.warehouseUg',{
        url: '/expectedUg',
        controller: 'ExpectedUgController',
        templateUrl : 'modules/main/views/warehouseUg.html'
    }).state('home.settings',{
        url:'/settings',
        controller:'SettingsController',
        templateUrl: 'modules/main/views/settings.html'
    }).state('home.messages',{
        url : '/messages',
        controller : 'MessagesController',
        templateUrl : 'modules/main/views/messages.html'
    }).state('home.warehouseChina',{
        url : '/warehouse_china',
        controller : 'WarehouseChinaController',
        templateUrl : 'modules/main/views/warehouseChina.html'
    })
}]);