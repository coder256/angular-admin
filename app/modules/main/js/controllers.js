/**
 * Created by Andrew on 9/5/16.
 */
angular.module('masikini_admin.main.controllers', [])
    .controller('HomeController',['$state','dataStore','$scope','userService','$rootScope','authorizer','$interval','urlProvider','$location', function ($state,dataStore,$scope,userService,$rootScope,authorizer,$interval,urlProvider,$location) {
        //console.log('The user',user);
        $rootScope.counter = urlProvider.session_timeout;

        $scope.warehouse_ADMIN = true;
                $scope.warehouse_UG = true;
                $scope.warehouse_USA = true;
                $scope.warehouse_CHINA = true;
        //switch (user.role){
        //    case "warehouse_USA":
        //        $scope.warehouse_USA = true;
        //        break;
        //    case "warehouse_UG":
        //        $scope.warehouse_UG = true;
        //        break;
        //    default :
        //        $scope.warehouse_ADMIN = true;
        //        $scope.warehouse_UG = true;
        //        $scope.warehouse_USA = true;
        //        $scope.warehouse_CHINA = true;
        //        break;
        //}

        $scope.logout = function () {
            //scrub info
            userService.user = undefined;
            dataStore.removeUser();
            authorizer.logout()
                .then(function (response) {
                    if(response.status){
                        dataStore.remove("returnTo");
                        dataStore.localStore("returnTo",$location.path());
                        $state.go('auth.login');
                    }else{
                        console.log('logout failed');
                    }
                });
        };

        var timer = $interval(function () {
            $rootScope.counter += -1;
            //console.log('counter ::', $rootScope.counter);
            //console.log('Current path ::',$location.path());
            if($rootScope.counter < 0){
                console.log("Session timed out :::",new Date().toLocaleTimeString());
                $interval.cancel(timer);
                dataStore.localStore("session_timedout",true);
                $scope.logout();
            }

        }, 1000);

        $scope.$on('session_checkin', function (event,data) {
            //$scope.logout();
            //$interval.cancel(timer);
            $rootScope.counter = urlProvider.session_timeout;
            console.log("Session Start :::",new Date().toLocaleTimeString());
        });

    }])
    .controller('DashboardController',['$state','$scope', function ($state,$scope) {
        //console.log('The user');
        $scope.title = "Dashboard";
        $scope.hhead = "head";
    }])
    .controller('UsersController',['$state','$scope','dataService','NgTableParams','$uibModal','transactionService', function ($state,$scope,dataService,NgTableParams,$uibModal,transactionService) {
        //console.log('The user');
        $scope.title = "Users";
        $scope.usr = {};
        $scope.setCur = function(usr){
            $scope.usr = usr;
        };

        $scope.getUsers = function () {
            $scope.loading = true;
            dataService.getUsers()
                .then(function (response) {
                    $scope.loading = false;
                    console.log('user sresp',response);
                    if(response.status){
                        $scope.callSuccess = true;
                        $scope.callFail = false;
                        $scope.users = response.results;
                        $scope.total = $scope.users.length;

                        $scope.userParams = new  NgTableParams({},{
                            dataset : $scope.users
                        });
                    }else{
                        $scope.callSuccess = false;
                        $scope.callFail = true;
                        $scope.callError = response.message;
                    }
                }, function (err) {
                    $scope.loading = false;
                    $scope.callSuccess = false;
                    $scope.callFail = true;
                    $scope.callError = "Check your internet connection :: "+err;
                })
        };

        $scope.message = function (size, parentSelector) {
            var parentElem = parentSelector ?
                angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
            var modalInstance = $uibModal.open({
                animation: $scope.animationsEnabled,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'myModalContent.html',
                //controller: function ($uibModalInstance, $scope, usr, transactionService) {
                controller: ['$uibModalInstance', '$scope', 'usr', 'transactionService',function ($uibModalInstance, $scope, usr, transactionService) {
                    $scope.usr = usr;
                    $scope.msg = {};
                    $scope.m_cancelText = "Cancel";
                    //console.log('Current user ::',$scope.usr);
                    $scope.ok = function () {
                        $scope.msg.userId = usr.email;
                        $scope.m_call = true;
                        //$uibModalInstance.close($ctrl.selected.item);
                        //console.log("Message request ::",$scope.msg);
                        //$uibModalInstance.close();
                        $scope.m_loading = true;
                        transactionService.newNotification($scope.msg)
                            .then(function(response){
                                //console.log(response);
                                $scope.m_cancelText = "Close";
                                $scope.m_loading = false;
                                if(response.status){
                                    $scope.m_callSuccess = true;
                                }else{
                                    $scope.m_callFail = true;
                                    $scope.m_callError = response.message;
                                }
                            })
                    };

                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                        $scope.m_call = false;
                    };
                }],
                size: size,
                appendTo: parentElem,
                resolve: {
                    usr: function () {
                        return $scope.usr;
                    },
                    transactionService: function(){
                        return transactionService;
                    }
                }
            });

            modalInstance.result.then(function (selectedItem) {
                //$ctrl.selected = selectedItem;
            }, function () {
                //console.log('Modal dismissed at: ' + new Date());
            });
        };

    }])
    .controller('UploadController',['$scope','$state','Upload', function ($scope,$state,Upload) {
        //console.log('upload controller running');
        $scope.title = "Upload Masikini Images";

        $scope.upload = function (file) {
            console.m_loading = true;
            Upload.upload({
                url: '../../api/v1/saveImage',
                data: {
                    file: file,
                    'type': $scope.imgType,
                    'title':$scope.imgTitle,
                    'category':$scope.category,
                    'placement':1
                }
            }).then(function (resp) {
                //console.log('Success ' + resp.config.data.file.name + 'uploaded. Response: ' + resp.data);
                console.log("Image upload response :: ",resp.data);
                $scope.m_loading = false;
                $scope.picFile = null;
                $scope.message = resp.data.message;
                if(resp.data.status){
                    $scope.callSuccess = true;
                    $scope.callFail = false;
                }else{
                    $scope.callFail = true;
                    $scope.callSuccess = false;
                }
            }, function (resp) {
                //console.log('Error status: ' + resp.status);
                console.log('Error status ::',resp);
            }, function (evt) {
                var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
            });
        };

        $scope.w_upload = function (file) {
            Upload.upload({
                //url: 'http://localhost:8888/masikini/api/v1/saveImage',
                url: '../../api/v1/saveImage',
                data: {
                    file: file,
                    'type': $scope.w_imgType,
                    'title':$scope.w_imgTitle,
                    'placement':$scope.w_placement
                }
            }).then(function (resp) {
                //console.log('Success ' + resp.config.data.file.name + 'uploaded. Response: ' + resp.data);
                $scope.loading = false;
                $scope.w_picFile = null;
                //console.log(resp);
                $scope.message = resp.data.message;
                if(resp.data.status){
                    $scope.callSuccess = true;
                }else{
                    $scope.callFail = true;
                }
            }, function (resp) {
                //console.log('Error status: ' + resp.status);
                console.log(resp);
            }, function (evt) {
                var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
            });
        };

    }])
    .controller('ImageController',['$scope','NgTableParams',function($scope,NgTableParams){
        $scope.title = "Images";
    }])
    .controller('SettingsController',['$scope','dataStore', function ($scope,dataStore) {
        $scope.title = "Settings";
    }])
    .controller('MessagesController',['$scope','theSocketer','dataService','NgTableParams','transactionService',function($scope,theSocketer,dataService,NgTableParams,transactionService){
        $scope.title = "Messages";

        $scope.message = function () {
            console.log('Sending socket message');
            theSocketer.get();
        };

        $scope.getMessages = function () {
            $scope.loading = true;
            dataService.messages()
                .then(function (response) {
                    $scope.loading = false;
                    if(response.status){
                        $scope.callSuccess = true;
                        $scope.callFail = false;
                        $scope.messages = response.result;
                        $scope.messageParams = new NgTableParams({}, {
                            dataset: $scope.messages
                        });
                        console.log('Messages ::',$scope.messages);
                    }else{
                        $scope.callFail = true;
                        $scope.callSucces = false;
                    }
                })
        };

        $scope.messageDetails = function (message) {
            $scope.mTitle = "Reply Message";
            $scope.broadcast = false;
            $scope.message = true;

            $scope.mFrom = message.name;
            $scope.mWhen = message.created;
            $scope.mMessage = message.message;
            $scope.mUserId = message.userId;
            $scope.mId = message.id;
            $scope.mSource = message.source;
        };
        $scope.broadcaster = function(){
            $scope.message = false;
            $scope.broadcast = true;
            $scope.mTitle = "Broadcast Message";
        };

        $scope.sendMessage = function () {
            //send message
            console.log('Submitting',$scope.mUserId+'--'+$scope.mReply);
            $scope.m_loading = true;
            transactionService.sendMessage($scope.mUserId,$scope.mReply,$scope.mSource)
                .then(function (response) {
                    $scope.m_loading = false;
                    console.log('Message response ::',response);
                    if(response.status){
                        $scope.mReply = '';
                        $scope.m_callSuccess = true;
                        $scope.m_callFail = false;
                    }else{
                        $scope.m_callSuccess = false;
                        $scope.m_callFail = true;
                    }
            });
            //adminService.repliedMessage($scope.mId).success(function (data) {
            //    $scope.getMessages();
            //})
        };

    }]);