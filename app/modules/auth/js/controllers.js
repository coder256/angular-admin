/**
 * Created by Andrew on 9/7/16.
 */
angular.module('masikini_admin.auth.controllers',[])
    .controller('AuthController',['$scope','$rootScope', function ($scope,$rootScope) {
        //console.log('Running smoothly');
    }])
    .controller('LoginController',['$scope','$state','authorizer','dataStore','userService','$location', function ($scope,$state,authorizer,dataStore,userService,$location) {
        $scope.title = "Admin Login";
        $scope.credentials = {};
        $scope.session_timedout = dataStore.localGet("session_timedout");

        var tt = dataStore.localGet("returnTo");
        if(angular.isUndefined(tt) || tt === "" || tt === null){
            console.log("passed");
        }

        $scope.login = function(){
            $scope.loading = true;
            dataStore.remove('session_timedout');
            dataStore.localStore('session_timedout',false);
        }
}]);