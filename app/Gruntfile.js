/**
 * Created by Andrew on 9/13/16.
 */
module.exports = function(grunt) {

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        uglify: {
            options: {
                banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
            },
            my_target:{
                files: {
                    'js/masikini_admin-scripts.min.js':[
                        'node_modules/jquery/dist/jquery.min.js',
                        'js/bootstrap.js',
                        'bower_components/angular/angular.js',
                        'bower_components/angular-route/angular-route.js',
                        'components/version/version.js',
                        'components/version/version-directive.js',
                        'components/version/interpolate-filter.js',
                        'node_modules/angular-ui-router/release/angular-ui-router.min.js',
                        'bower_components/angular-local-storage/dist/angular-local-storage.min.js',
                        'bower_components/ng-file-upload/ng-file-upload-shim.js',
                        'bower_components/ng-file-upload/ng-file-upload.js',
                        'bower_components/ng-table/dist/ng-table.js',
                        'bower_components/angular-xeditable/dist/js/xeditable.min.js',
                        'bower_components/angular-websocket/dist/angular-websocket.min.js',
                        'node_modules/angular-ui-bootstrap/dist/ui-bootstrap-tpls.js'
                    ],
                    'js/admin_angular.min.js':[
                        'js/app.js',
                        'js/services.js',
                        'modules/main/mainModule.js',
                        'modules/main/js/controllers.js',
                        'modules/main/js/services.js',
                        'modules/main/js/filters.js',
                        'modules/main/js/directives.js',
                        'modules/auth/authModule.js',
                        'modules/auth/js/controllers.js',
                        'modules/auth/js/services.js'
                    ]
                }
            }
        },
        cssmin: {
            options: {
                shorthandCompacting: false,
                roundingPrecision: -1
            },
            target: {
                files: {
                    'css/masikini_admin-styles.css': [
                        'bower_components/html5-boilerplate/dist/css/normalize.css',
                        'bower_components/html5-boilerplate/dist/css/main.css',
                        'css/bootstrap.css',
                        'css/signin.css',
                        'css/sb-admin.css',
                        'css/app.css',
                        'bower_components/ng-table/dist/ng-table.css',
                        'bower_components/angular-xeditable/dist/css/xeditable.min.css'
                    ]
                }
            }
        }
    });

    // Load the plugin that provides the "uglify" task.
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');

    // Default task(s).
    grunt.registerTask('default', ['uglify','cssmin']);

};