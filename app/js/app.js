'use strict';

// Declare app level module which depends on views, and components
angular.module('masikini_admin', [
    'ui.router', 'LocalStorageModule','masikini_admin.main','ngFileUpload','masikini_admin.services','ngTable','masikini_admin.auth','xeditable','ngWebSocket','ui.bootstrap'
])
    .config(['$locationProvider', 'localStorageServiceProvider','$httpProvider', function ($locationProvider, localStorageServiceProvider,$httpProvider) {
        //$locationProvider.hashPrefix('!');

        //configure local storage
        localStorageServiceProvider
            .setPrefix('Masikini')
            .setStorageType('sessionStorage');

        //configure $http
        $httpProvider.defaults.headers.post = {
            'Content-Type': 'application/x-www-form-urlencoded'
        };
        $httpProvider.defaults.headers.put = {
            'Content-Type': 'application/x-www-form-urlencoded'
        };

        //set interceptors
        $httpProvider.interceptors.push('httpInterceptor');

    }])
    .run(['$state','$rootScope','userService','dataStore','editableOptions', function ($state,$rootScope,userService,dataStore,editableOptions) {
        console.log('Masikini admin running');

        //load default view
        $state.go('home.dashboard');

        $rootScope.$on('$stateChangeError', function(event, toState, toParams, fromState, fromParams, error) {

            if(error.unAuthorized) {
                console.log('Unauthorized');
                $state.go('auth.login');
            }
            else if(error.authorized){
                console.log('Authorized');
                $state.go('home.dashboard');
            }
        });

        userService.user = dataStore.getUser();

        //configure xeditable
        editableOptions.theme = 'bs3';

    }]);
