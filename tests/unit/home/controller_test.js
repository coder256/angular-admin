/**
 * Created by Andrew on 10/18/16.
 */

describe('Controllers',function(){
    beforeEach(module('ui.router'));
    beforeEach(module('masikini_admin.services'));
    beforeEach(module('masikini_admin.main.services'));
    beforeEach(module('masikini_admin.main.controllers'));
    var scope;
    describe('DashboardController',function(){
        var myctrl;
        //var scope;
        beforeEach(inject(function ($rootScope,$controller) {
            scope = $rootScope.$new();
            myctrl = $controller('DashboardController',{
                $scope : scope
            });
        }));
        //test
        it('title should be Dashboard',function(){
            expect(scope.title)
                .toBeDefined();
        })
    });
});

describe('SettingsController', function () {
    beforeEach(module('ui.router'));
    beforeEach(module('LocalStorageModule'));
    beforeEach(module('masikini_admin.services'));
    beforeEach(module('masikini_admin.main.services'));
    beforeEach(module('masikini_admin.main.controllers'));

    var dataStore;
    beforeEach(module('masikini_admin.services', function ($provide) {
        dataStore = {
            getUser : function() {}
        };
        spyOn(dataStore,'getUser').and.returnValue({role:"warehouse_USA"});
        $provide.value('getUser',dataStore);
    }));

    var d;
    var mockSvc;
    var mockDataStore;
    beforeEach(inject(function($q,dataStore){
        d = $q.defer();
        mockDataStore = dataStore;
        //spyOn(mockDataStore,'getUser').and.returnValue(d.promise);
        spyOn(mockDataStore,'getUser').and.returnValue({role:"warehouse_USA"});
        //scope.getUser();
    }));
    beforeEach(inject(function($q,dataService){
        d = $q.defer();
        mockSvc = dataService;
        spyOn(mockSvc,'rates').and.returnValue(d.promise);
        //spyOn(mockSvc,'getRates').andCallThrough();
    }));
    var myctrl;
    beforeEach(inject(function($controller,$rootScope,dataStore){
        scope = $rootScope.$new();
        myctrl = $controller('SettingsController',{
            $scope : scope,
            dataService : mockSvc,
            dataStore : mockDataStore
        });
    }));

    it('title should be set', function () {
        expect(scope.title).toBe('Settings');
    });

    it('should call getRates method', function () {
        //scope.rt_callSuccess = false;
        scope.getRates();
        scope.resetData.c_rpassword = {};
        //scope.resetForm.c_rpassword.$setValidity(true);
        d.resolve('Success');
        scope.$digest();
        expect(mockSvc.rates).toHaveBeenCalled();
        expect(mockDataStore.getUser).toHaveBeenCalled();
        expect(scope.rt_callSuccess).toBe(true);
    });

    //describe('http call',function(){
    //    //it('should call ')
    //})
});
