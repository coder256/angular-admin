/**
 * Created by Andrew on 10/18/16.
 */

describe('Filters',function(){ //describe object type
    beforeEach(module('masikini_admin.filters')); //load module
    describe('reverse',function(){ //describe app name
        var reverse;
        beforeEach(inject(function($filter){ //initialize filter
            reverse = $filter('reverse',{});
        }));
        it('Should reverse a string',function(){ //write test
            expect(reverse('data'))
                .toBe('atad'); //should pass
            expect(reverse('andrew'))
                .toBe('werdna'); //should pass
        });
    })
});
